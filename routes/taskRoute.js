// this document contain all the endpoints for our application (also http methods)

const express = require("express");
const router = express.Router();
const taskController = require ("../controllers/taskController.js");

router.get("/viewTasks", (req,res) =>{

	//Invokes the ""

													// returns result from our controller
	taskController.getAllTask().then(resultFromController => res.send(resultFromController));
})

router.post("/addNewTask", (req,res) => {
	taskController.createTask(req.body).then(result => res.send(result));
})
module.exports = router;
