/*

GitBash:
		npm init -y 
		npm i expresss 
		npm i mongoose 
		touch .gitignore >> content: node_modules
*/

// dependencies 
const express = require("express");
const mongoose = require("mongoose");

/* 

models folder >> task.js
contollers folder >> taskController.js
routes folder >> taskRoute.js

*/ 

const taskRoute = require ("./routes/taskRoute.js");


// Server Setup

const app = express();
const port = 3001;

//Middlewares

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// DB connection 

mongoose.connect("mongodb+srv://admin:admin@b218-to-do.lspbs3i.mongodb.net/toDo?retryWrites=true&w=majority",

	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

app.use("/tasks", taskRoute);

app.listen(port, () => console.log (`Server running at port ${port}`));

// GitBash:
// Start node application:
// nodmeon index.js 
