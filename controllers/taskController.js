const Task = require("../models/task.js");

module.exports.getAllTask = () => {
	return Task.find({}).then(result =>{
		return result; 
	})
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name : requestBody.name 
	})

	return newTask.save().then((task, error) => {
		if(error) {
			console.log(error);
			return "Error detected";
		}
		else {
			return task;
		}
	})
}